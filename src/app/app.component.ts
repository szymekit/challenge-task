import { Component } from '@angular/core';

export enum WidgetTypes {
  TOP_RISK_DEVICE = 'TOP_RISK_DEVICE',
  TOP_ACTIVE_USERS = 'TOP_ACTIVE_USERS',
  EVENTS_TIMELINE = 'EVENTS_TIMELINE'
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public widgetTypes = WidgetTypes;
}
