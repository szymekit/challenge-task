import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TopActiveUsersComponent } from './widgets/top-active-users/top-active-users.component';
import { TopRiskDeviceComponent } from './widgets/top-risk-device/top-risk-device.component';
import { EventsTimelineComponent } from './widgets/events-timeline/events-timeline.component';
import { WidgetsGridComponent } from './widgets-grid/widgets-grid.component';
import { WidgetComponent } from './widget/widget.component';

@NgModule({
  declarations: [
    AppComponent,
    TopActiveUsersComponent,
    TopRiskDeviceComponent,
    EventsTimelineComponent,
    WidgetsGridComponent,
    WidgetComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
