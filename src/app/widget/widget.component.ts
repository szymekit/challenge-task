import {Component, Input, OnInit} from '@angular/core';
import {WidgetTypes} from "../app.component";

@Component({
  selector: 'app-widget',
  templateUrl: './widget.component.html',
  styleUrls: ['./widget.component.scss']
})
export class WidgetComponent implements OnInit {

  @Input() public name!: string;
  @Input() public type!: string;

  public widgetTypes = WidgetTypes;

  constructor() { }

  ngOnInit(): void {
  }

}
