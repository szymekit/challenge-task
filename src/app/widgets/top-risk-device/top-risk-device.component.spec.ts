import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopRiskDeviceComponent } from './top-risk-device.component';

describe('TopRiskDeviceComponent', () => {
  let component: TopRiskDeviceComponent;
  let fixture: ComponentFixture<TopRiskDeviceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TopRiskDeviceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TopRiskDeviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
