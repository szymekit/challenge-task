import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-events-timeline',
  templateUrl: './events-timeline.component.html',
  styleUrls: ['./events-timeline.component.scss']
})
export class EventsTimelineComponent implements OnInit {

  isLoading: boolean = false;
  hasError: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  loadingData() {
    this.isLoading = true;
  }

}
