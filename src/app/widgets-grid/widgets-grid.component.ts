import {Component, ContentChildren, OnInit, QueryList} from '@angular/core';
import {WidgetComponent} from "../widget/widget.component";
import {WidgetTypes} from "../app.component";

@Component({
  selector: 'app-widgets-grid',
  templateUrl: './widgets-grid.component.html',
  styleUrls: ['./widgets-grid.component.scss']
})
export class WidgetsGridComponent implements OnInit {

  @ContentChildren('widget') widgets!: QueryList<WidgetComponent>;

  constructor() { }

  ngOnInit(): void {
    setTimeout(() => {
      console.log(this.widgets)
    })
  }

  show(w: any): void {
    console.log(w)
  }

}
